// Yarn add
yarn add firebase react-dropzone history redux react-redux react-router react-router-dom recompose redux-form redux-logger redux-thunk connected-react-router
HTML

<!-- public/index.html -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.1/css/bulma.min.css" />
<link href="https://fonts.googleapis.com/css?family=Quicksand:300,700" rel="stylesheet">

<!------------------------------------------------------------------------------------------------------------------------->
// stores/index.js

import thunk from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import createLogger from 'redux-logger'
import { routerMiddleware } from 'connected-react-router'
import { createStore, applyMiddleware } from 'redux'

import rootReducers from '../reducers'

export const history = createHistory()
const historyRouterMiddleware = routerMiddleware(history)

const store = createStore(
  rootReducers(history),
  applyMiddleware(thunk, historyRouterMiddleware, createLogger)
)

export default store


<!------------------------------------------------------------------------------------------------------------------------->
// reducers/index.js

import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { reducer as form } from 'redux-form'

export default history =>
  combineReducers({
    router: connectRouter(history),
    form
  })
  
<!------------------------------------------------------------------------------------------------------------------------->
// firebase/index.js

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const config = {} // Replace your firebase project

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

db.settings({
  timestampsInSnapshots: true
})

export default {
  db,
  auth,
  storage
}  
<!------------------------------------------------------------------------------------------------------------------------->
// index.js

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'

import store, { history } from './stores'

import './index.css'
import App from './App'

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)

<!------------------------------------------------------------------------------------------------------------------------->
/* index.css */

body {
  margin: 0;
  padding: 0;
  font-family: 'Quicksand', sans-serif;
}

.navbar {
  position: absolute;
  width: 100%;
  padding: 12px;
  z-index: 1;
}

.navbar.is-transparent {
  background-color: transparent;
  background-image: none;
}

.btn-logout {
  margin-left: 8px;
}

figure.image {
  border: none;
  transition: box-shadow 0.1s;
}

figure.image:hover {
  box-shadow: 0 0 16px rgba(55, 55, 55, 0.2);
}

/* Product List */
.box > .columns {
  display: flex;
  justify-content: center;
  align-items: center;
}

.product-box {
  display: flex;
  align-items: center;
}

.product-box img {
  width: 180px;
  height: auto;
}

.product-description {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
}

.product-description p {
  margin-bottom: 1em;
}

.product-box-meta {
  margin-left: 12px;
}

.price-text {
  color: #00d1b2;
  font-weight: bold;
}

/* Dropzone */
.preview-image-box {
  border: 1px solid #dcdcdc;
  width: 100%;
  min-height: 240px;
  max-height: 320px;
  margin-top: 12px;
  object-fit: cover;
}

.input-dropzone {
  border: 1px dashed #888;
  padding: 8px;
  width: 320px;
}

