import { FETCH_PRODUCTS } from '../../actions/products'

export const initialState = {
  isFetching: false,
  error: null,
  data: []
}

const createReducer = (Action = {}, defaultState = initialState) => (
  state = defaultState,
  { type, payload, error } = Action
) => {
  switch (type) {
    case type.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case type.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload
      }
    case type.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        data: []
      }
    default:
      return state
  }
}

createReducer(FETCH_PRODUCTS)

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case FETCH_PRODUCTS.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case FETCH_PRODUCTS.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload
      }
    case FETCH_PRODUCTS.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        data: []
      }
    default:
      return state
  }
}
