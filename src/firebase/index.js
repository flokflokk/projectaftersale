import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

// your firebase config
const config = {
  apiKey: "AIzaSyB4mEoz0tBVhACAmW_QYlpRTZjplMI0zRw",
  authDomain: "projectaftersale.firebaseapp.com",
  databaseURL: "https://projectaftersale.firebaseio.com",
  projectId: "projectaftersale",
  storageBucket: "projectaftersale.appspot.com",
  messagingSenderId: "908334711446"
} 

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

db.settings({
  timestampsInSnapshots: true
})

export default {
  db,
  auth,
  storage
}
